<?php
namespace app\index\controller;

use app\BaseController;

class Index extends BaseController
{
    public function index()
    {
      if(request()->isPost()){
        //获取表单上传文件
        $file = request()->file('image');
        $savename = \think\facade\Filesystem::disk('public')->putFile('cark',$file);
      }
        return view();
    }

    public function queueTest()
    {
        $data = ['order_no'=>rand(100000,999999)];
        $this->add($data['order_no']);
        $data = json_encode($data);
        $res = \think\facade\Queue::push('Job',$data,$queue=null);
        var_dump($res);
    }

    public function add($orderNo)
    {
        $data = [
            'order_no'=>$orderNo,
            'msg'=>$orderNo,
            'create_time'=>date('Y-m-d H:i:s')
        ];
        \think\facade\Db::name('tp6_test')->insert($data);
    }
}
