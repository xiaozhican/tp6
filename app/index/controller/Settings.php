<?php


namespace app\index\controller;


use think\worker\Server;
use Workerman\Lib\Timer;

class Settings extends Server
{
    public function __construct()
    {
        parent::__construct();
        $this->onWorkerStart();
    }
    public function add_timer()
    {
        //60秒执行一次定时任务
        Timer::add(60,array($this,'index'),array(),true);
    }
    public function onWorkerStart(){
        $this->add_timer();
    }

    public function onMessage($connection,$data)
    {
        $message = [
            'application_ids.require' => '应用类型必须选定',
            'upgrade_start.require' => '升级开始时间不能为空',
            'upgrade_start.dateFormat' => '时间格式不正确',
            'upgrade_end.dateFormat' => '时间格式不正确',
            'upgrade_end.gt' => '结束时间必须大于开始时间',
            'upgrade_end.require' => '升级结束时间不能为空',
            'tips.require' => '升级备注不能为空',
            'users_ids.require' => '请选择升级用户或类型',
            'close_type.require' => '必须选择关闭某个端',
        ];
        $connection->send(json_encode($message));
    }

    public function index()
    {
        file_put_contents('./a.log',date('Y-m-d H:i:s',time()).'执行了一次定时任务。'."\r\n",FILE_APPEND);
        //if()
    }
}