<?php
namespace app\job;
use app\BaseController;
use think\Db;

class Job extends BaseController{
    public function fire(\think\queue\Job $job,$data)
    {
        file_put_contents('./job.log',date('Y-m-d H:i:s').'执行了队列任务'."\r\n",FILE_APPEND);
        $data = json_decode($data,true);
        if($this->jobDone($data)){
            //$job->delete();
            file_put_contents('./job.log',date('Y-m-d H:i:s').'删除队列任务'."\r\n",FILE_APPEND);
        }else{
            $job->release(5);
        }
        if($job->attempts() > 3){
            file_put_contents('./job.log',date('Y-m-d H:i:s').'检查任务'."\r\n",FILE_APPEND);
        }
        //$job->release(10);
    }

    public function jobDone($data)
    {
        file_put_contents('./job.log',date('Y-m-d H:i:s').'队列状态'."\r\n",FILE_APPEND);
        return \think\facade\Db::name('tp6_test')->where('order_no',$data['order_no'])->update(['status'=>2]);
    }

    public function failed($data)
    {
        file_put_contents('./job.log',date('Y-m-d H:i:s').'队列任务达到最大重试次数，失败'."\r\n",FILE_APPEND);
    }
}